// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CreateSaveAsset.generated.h"

/**
 * 
 */
UCLASS()
class LEVELGENERATIONTOOL_API UCreateSaveAsset : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "DGTools", meta = (Keywords = "Save"))
	static bool SaveLevelData(FString SaveDirectory, FString FileName, TArray<FString> ObjData, bool AllowOverwrite);

	UFUNCTION(BlueprintCallable, Category = "DGTools", meta = (Keywords = "Math"))
	static FQuat ConvertRotToQuat(FRotator ObjRotation);
};
