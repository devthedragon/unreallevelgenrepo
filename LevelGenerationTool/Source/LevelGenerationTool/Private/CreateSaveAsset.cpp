// Fill out your copyright notice in the Description page of Project Settings.


#include "CreateSaveAsset.h"
#include "Misc/FileHelper.h"
#include "HAL/PlatformFilemanager.h"

bool UCreateSaveAsset::SaveLevelData(FString SaveDirectory, FString FileName, TArray<FString> ObjData, bool AllowOverwrite = false)
{
	SaveDirectory += "\\";
	SaveDirectory += FileName;

	if (!AllowOverwrite) 
	{
		if (FPlatformFileManager::Get().GetPlatformFile().FileExists(*SaveDirectory)) 
		{
			return false;
		}
	}

	FString FinalString = "";
	int TempInt = 0;
	for (FString& Each : ObjData) 
	{
		if (TempInt != 0) {
			FinalString += " " + FString::FromInt(TempInt) + ",";
		}
		FinalString += Each;
		FinalString += LINE_TERMINATOR;
		TempInt++;
	}

	return FFileHelper::SaveStringToFile(FinalString, *SaveDirectory);
}

FQuat UCreateSaveAsset::ConvertRotToQuat(FRotator ObjRotation) 
{
	return ObjRotation.Quaternion();
}
