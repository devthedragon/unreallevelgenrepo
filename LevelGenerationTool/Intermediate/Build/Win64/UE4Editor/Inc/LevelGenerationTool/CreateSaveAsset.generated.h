// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRotator;
struct FQuat;
#ifdef LEVELGENERATIONTOOL_CreateSaveAsset_generated_h
#error "CreateSaveAsset.generated.h already included, missing '#pragma once' in CreateSaveAsset.h"
#endif
#define LEVELGENERATIONTOOL_CreateSaveAsset_generated_h

#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConvertRotToQuat) \
	{ \
		P_GET_STRUCT(FRotator,Z_Param_ObjRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FQuat*)Z_Param__Result=UCreateSaveAsset::ConvertRotToQuat(Z_Param_ObjRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSaveLevelData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveDirectory); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_TARRAY(FString,Z_Param_ObjData); \
		P_GET_UBOOL(Z_Param_AllowOverwrite); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCreateSaveAsset::SaveLevelData(Z_Param_SaveDirectory,Z_Param_FileName,Z_Param_ObjData,Z_Param_AllowOverwrite); \
		P_NATIVE_END; \
	}


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConvertRotToQuat) \
	{ \
		P_GET_STRUCT(FRotator,Z_Param_ObjRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FQuat*)Z_Param__Result=UCreateSaveAsset::ConvertRotToQuat(Z_Param_ObjRotation); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSaveLevelData) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveDirectory); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_TARRAY(FString,Z_Param_ObjData); \
		P_GET_UBOOL(Z_Param_AllowOverwrite); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UCreateSaveAsset::SaveLevelData(Z_Param_SaveDirectory,Z_Param_FileName,Z_Param_ObjData,Z_Param_AllowOverwrite); \
		P_NATIVE_END; \
	}


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCreateSaveAsset(); \
	friend struct Z_Construct_UClass_UCreateSaveAsset_Statics; \
public: \
	DECLARE_CLASS(UCreateSaveAsset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelGenerationTool"), NO_API) \
	DECLARE_SERIALIZER(UCreateSaveAsset)


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCreateSaveAsset(); \
	friend struct Z_Construct_UClass_UCreateSaveAsset_Statics; \
public: \
	DECLARE_CLASS(UCreateSaveAsset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelGenerationTool"), NO_API) \
	DECLARE_SERIALIZER(UCreateSaveAsset)


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCreateSaveAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCreateSaveAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCreateSaveAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCreateSaveAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCreateSaveAsset(UCreateSaveAsset&&); \
	NO_API UCreateSaveAsset(const UCreateSaveAsset&); \
public:


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCreateSaveAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCreateSaveAsset(UCreateSaveAsset&&); \
	NO_API UCreateSaveAsset(const UCreateSaveAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCreateSaveAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCreateSaveAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCreateSaveAsset)


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_PRIVATE_PROPERTY_OFFSET
#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_12_PROLOG
#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_RPC_WRAPPERS \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_INCLASS \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_INCLASS_NO_PURE_DECLS \
	LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELGENERATIONTOOL_API UClass* StaticClass<class UCreateSaveAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LevelGenerationTool_Source_LevelGenerationTool_Public_CreateSaveAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
