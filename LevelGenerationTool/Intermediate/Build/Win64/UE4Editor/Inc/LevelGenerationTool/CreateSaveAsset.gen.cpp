// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelGenerationTool/Public/CreateSaveAsset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCreateSaveAsset() {}
// Cross Module References
	LEVELGENERATIONTOOL_API UClass* Z_Construct_UClass_UCreateSaveAsset_NoRegister();
	LEVELGENERATIONTOOL_API UClass* Z_Construct_UClass_UCreateSaveAsset();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LevelGenerationTool();
	LEVELGENERATIONTOOL_API UFunction* Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	LEVELGENERATIONTOOL_API UFunction* Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData();
// End Cross Module References
	void UCreateSaveAsset::StaticRegisterNativesUCreateSaveAsset()
	{
		UClass* Class = UCreateSaveAsset::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ConvertRotToQuat", &UCreateSaveAsset::execConvertRotToQuat },
			{ "SaveLevelData", &UCreateSaveAsset::execSaveLevelData },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics
	{
		struct CreateSaveAsset_eventConvertRotToQuat_Parms
		{
			FRotator ObjRotation;
			FQuat ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CreateSaveAsset_eventConvertRotToQuat_Parms, ReturnValue), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::NewProp_ObjRotation = { "ObjRotation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CreateSaveAsset_eventConvertRotToQuat_Parms, ObjRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::NewProp_ObjRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::Function_MetaDataParams[] = {
		{ "Category", "DGTools" },
		{ "Keywords", "Math" },
		{ "ModuleRelativePath", "Public/CreateSaveAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCreateSaveAsset, nullptr, "ConvertRotToQuat", sizeof(CreateSaveAsset_eventConvertRotToQuat_Parms), Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04842401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics
	{
		struct CreateSaveAsset_eventSaveLevelData_Parms
		{
			FString SaveDirectory;
			FString FileName;
			TArray<FString> ObjData;
			bool AllowOverwrite;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static void NewProp_AllowOverwrite_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AllowOverwrite;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjData;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjData_Inner;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveDirectory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CreateSaveAsset_eventSaveLevelData_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CreateSaveAsset_eventSaveLevelData_Parms), &Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_AllowOverwrite_SetBit(void* Obj)
	{
		((CreateSaveAsset_eventSaveLevelData_Parms*)Obj)->AllowOverwrite = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_AllowOverwrite = { "AllowOverwrite", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CreateSaveAsset_eventSaveLevelData_Parms), &Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_AllowOverwrite_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ObjData = { "ObjData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CreateSaveAsset_eventSaveLevelData_Parms, ObjData), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ObjData_Inner = { "ObjData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CreateSaveAsset_eventSaveLevelData_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_SaveDirectory = { "SaveDirectory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CreateSaveAsset_eventSaveLevelData_Parms, SaveDirectory), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_AllowOverwrite,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ObjData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_ObjData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_FileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::NewProp_SaveDirectory,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::Function_MetaDataParams[] = {
		{ "Category", "DGTools" },
		{ "Keywords", "Save" },
		{ "ModuleRelativePath", "Public/CreateSaveAsset.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCreateSaveAsset, nullptr, "SaveLevelData", sizeof(CreateSaveAsset_eventSaveLevelData_Parms), Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCreateSaveAsset_NoRegister()
	{
		return UCreateSaveAsset::StaticClass();
	}
	struct Z_Construct_UClass_UCreateSaveAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCreateSaveAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelGenerationTool,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCreateSaveAsset_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCreateSaveAsset_ConvertRotToQuat, "ConvertRotToQuat" }, // 926975522
		{ &Z_Construct_UFunction_UCreateSaveAsset_SaveLevelData, "SaveLevelData" }, // 34036070
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCreateSaveAsset_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CreateSaveAsset.h" },
		{ "ModuleRelativePath", "Public/CreateSaveAsset.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCreateSaveAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCreateSaveAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCreateSaveAsset_Statics::ClassParams = {
		&UCreateSaveAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCreateSaveAsset_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCreateSaveAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCreateSaveAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCreateSaveAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCreateSaveAsset, 3462101116);
	template<> LEVELGENERATIONTOOL_API UClass* StaticClass<UCreateSaveAsset>()
	{
		return UCreateSaveAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCreateSaveAsset(Z_Construct_UClass_UCreateSaveAsset, &UCreateSaveAsset::StaticClass, TEXT("/Script/LevelGenerationTool"), TEXT("UCreateSaveAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCreateSaveAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
